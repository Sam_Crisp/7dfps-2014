﻿using UnityEngine;
using System.Collections;

public class Interactor : MonoBehaviour {

	public UnityEngine.UI.Text text;
	public UnityEngine.UI.Image reticuleSprite;
	public Reticule reticule;

	private const int layerMask = 1 << 13 | 1 << 11;
	private const int damLayerMask = 1 << 11 | 1 << 14; // Only environment and damage recievers
	private const float maxRange = 50.0f;

	void Update () {
		RaycastHit hit;
		Ray ray = Camera.main.ScreenPointToRay(new Vector2(Screen.width * 0.5f, Screen.height * 0.5f));
		Debug.DrawRay(ray.origin, ray.direction, Color.cyan);
		if (Physics.Raycast(ray, out hit, maxRange, layerMask))
		{
			// Check for interactable
			Interactable i = hit.collider.GetComponent<Interactable>();
			if (i != null && Vector3.Distance(Game.Player.position, i.transform.position) < i.range)
			{
				UseReticule(i.MouseOver());
				if (Input.GetKeyDown(KeyCode.E)) i.Interact();
				return;
			}
		}

		// Check for potential damage		
		if (Physics.SphereCast(ray, 0.2f, out hit, 100.0f, damLayerMask))
		{
			// Handle damage
			DamageHandler handler = hit.collider.GetComponent(typeof(DamageHandler)) as DamageHandler;
			if (handler != null) {
				DefaultReticule(true);
				return;
			}
		}

		DefaultReticule();
	}

	void DefaultReticule(bool red = false)
	{
		text.text = "";
		Reticule.SetReticule(red ? Reticule.ReticuleType.CrosshairRed : Reticule.ReticuleType.Crosshair);
	}

	void UseReticule(string useText)
	{
		string useKey = "(E)";
		text.text = useKey + " " + useText;
		Reticule.SetReticule(Reticule.ReticuleType.Hand);
	}
}
