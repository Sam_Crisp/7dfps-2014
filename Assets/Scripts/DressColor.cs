﻿using UnityEngine;
using System.Collections;

public class DressColor : MonoBehaviour {

	Material mat;
	LineOfSight los;
	
	void Awake () {
		mat = transform.Find("Mesh/Dress").renderer.material;
		los = GetComponent<LineOfSight>();
	}

	void Update () {
		mat.color = los.CanSeePlayer ? Color.green : Color.red;
	}
}
