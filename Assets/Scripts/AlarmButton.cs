﻿using UnityEngine;
using System.Collections;

public class AlarmButton : Interactable {
	public static bool Alarm = false;
	public bool pressed = false;

	private Transform button;
	private static AlarmLights[] lights;

	void Awake()
	{
		button = transform.GetChild(1);
		lights = FindObjectsOfType<AlarmLights>();
	}

	public override bool CanInteract {
		get {
			return !(Alarm || pressed);
		}
	}

	public override void Interact ()
	{
		SetAlarm (!Alarm);
	}

	public override string MouseOver ()
	{
		return Alarm ? "Turn Off Alarm" : "Turn On Alarm";
	}

	public void SetOffAlarm()
	{
		SetAlarm(true);
	}

	public void ShutDownAlarm()
	{
		SetAlarm(false);
	}

	void SetAlarm(bool on)
	{
		if (!pressed) StartCoroutine(AnimateButton());
		pressed = true;
		Alarm = on;
		foreach (AlarmLights l in lights)
			l.Set(on);
	}

	IEnumerator AnimateButton()
	{
		float speed = 5.0f;
		Vector3 originalPos = button.transform.localPosition;
		Vector3 targetPos = new Vector3(originalPos.x, originalPos.y, originalPos.z - 0.1f);
		while (button.transform.localPosition != targetPos)
		{
			button.transform.localPosition = Vector3.Lerp(button.transform.localPosition, targetPos, Time.deltaTime * speed);
			yield return null;
		}
		while (button.transform.localPosition != originalPos)
		{
			button.transform.localPosition = Vector3.Lerp(button.transform.localPosition, originalPos, Time.deltaTime * speed);
			yield return null;
		}
		pressed = false;
	}
}
