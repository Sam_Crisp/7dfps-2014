﻿using UnityEngine;
using System.Collections;

public class LocationalDamageHandler : MonoBehaviour, DamageHandler {

	public DamageLocation location;
	public float damageMultiplier = 1.0f;
	protected Health health;

	void Awake () {
		health = GetComponentInParent<Health>();
	}
	
	public void DoDamage(float damage)
	{
		health.Damage(damage * damageMultiplier, location);
	}
}

public enum DamageLocation
{
	Default, Head, Chest, Stomach, Foot, Back
}