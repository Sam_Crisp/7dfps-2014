﻿using UnityEngine;
using System.Collections;

public class ParticleSystemAutoDestroyer : MonoBehaviour {
	float startTime;
	void Start()
	{
		startTime = Time.time;
	}
	void Update () {
		if (particleSystem != null && particleSystem.particleCount == 0 && Time.time > startTime + particleSystem.duration)
			Destroy(gameObject);
	}
}
