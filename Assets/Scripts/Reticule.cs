﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Reticule : MonoBehaviour {

	public static bool Crosshair = false;
	public static ReticuleType Type;

	public Sprite crosshairSprite, handSprite, noSprite, crossSprite;

	protected static Image sprite;
	protected static Sprite _crosshairSprite, _handSprite, _noSprite, _crossSprite;

	protected void Awake()
	{
		if (sprite == null)
			sprite = GetComponent<Image>();
		if (crosshairSprite != null) _crosshairSprite = crosshairSprite;
		if (handSprite != null) _handSprite = handSprite;
		if (noSprite != null) _noSprite = noSprite;
		if (crossSprite != null) _crossSprite = crossSprite;
	}

	public enum ReticuleType
	{
		Crosshair, CrosshairRed, Hand, No
	}

	public static void SetReticule(ReticuleType type)
	{
		Reticule.Type = type;

		switch (type)
		{
		case ReticuleType.Crosshair:
			sprite.sprite = _crosshairSprite;
			sprite.color = Color.white;
			sprite.enabled = Crosshair;
			break;
		case ReticuleType.CrosshairRed:
			sprite.sprite = _crosshairSprite;
			sprite.color = Color.red;
			sprite.enabled = Crosshair;
			break;
		case ReticuleType.Hand:
			sprite.sprite = _handSprite;
			sprite.color = Color.yellow;
			sprite.enabled = true;
			break;
		default:
			sprite.enabled = false;
			break;
		}
	}
}
