﻿using UnityEngine;
using System.Collections.Generic;

public class WeaponController : MonoBehaviour {

	public UnityEngine.UI.Text text;
	public Transform pistolPrefab;
	public bool startWithWeapons = false;

	List<Weapon> weapons;
	Weapon currentWeapon = null;
	int currentWeaponIndex = 0;

	protected void Awake()
	{
		weapons = new List<Weapon>();
		if (startWithWeapons) PickUpWeapon(pistolPrefab, 6);
	}

	protected void Update()
	{
		// Handle reloading
		if (Input.GetKeyDown(KeyCode.R) && currentWeapon != null)
			currentWeapon.Reload();

		// Handle shooting
		else if (Input.GetMouseButtonDown(0) && currentWeapon != null)
			currentWeapon.Shoot();


		// Handle changing weapon via mousewheel
		else if (Input.GetAxis("Mouse ScrollWheel") > 0f)
		{
			SetWeapon(currentWeaponIndex + 1);
			//print (Input.GetAxis("Mouse ScrollWheel") + " Scrolled up");
		}
		else if (Input.GetAxis("Mouse ScrollWheel") < 0f)
		{
			SetWeapon(currentWeaponIndex - 1);
			//print (Input.GetAxis("Mouse ScrollWheel") + " Scrolled down");
		}

		// Changing weapon via num keys
		if (Input.GetKeyDown(KeyCode.Alpha1)) SetWeaponOrHolster(1);
		else if (Input.GetKeyDown(KeyCode.Alpha2)) SetWeaponOrHolster(2);

		// Set ammo text
		if (currentWeaponIndex > 0) text.text = currentWeapon.AmmoText();
		else text.text = "";
	}

	void SetWeaponOrHolster(int index)
	{
		if (currentWeaponIndex == index)
			SetWeapon(0);
		else SetWeapon(index);
	}

	void SetWeapon(int index)
	{
		// Check if weapon index is valid
		if (index >= 0 && index <= weapons.Count)
		{
			// Deactivate current weapon
			if (currentWeaponIndex > 0)
				weapons[currentWeaponIndex-1].gameObject.SetActive(false);

			// Update weapon index
			currentWeaponIndex = index;
			// Index of zero means no weapon is equipped
			if (index > 0) {
				currentWeapon = weapons[index-1];
				weapons[index-1].gameObject.SetActive(true);
				Reticule.Crosshair = true;
			}
			else {
				currentWeapon = null;
				Reticule.Crosshair = false;
			}
		}
	}

	public bool PickUpWeapon(Transform prefab, int ammoFallback)
	{
		// Check if the player already has the weapon
		System.Type t = prefab.GetComponent<Weapon>().GetType();
		if (weapons.Find(x => x.GetType() == t) != null)
			return PickUpAmmo(t, ammoFallback);

		Transform weapon = Instantiate(prefab) as Transform;
		weapon.transform.parent = transform;
		weapon.localPosition = Vector3.zero;
		weapon.localRotation = Quaternion.identity;
		weapons.Add (weapon.GetComponent<Weapon>());
		SetWeapon(weapons.Count);
		return true;
	}

	public bool PickUpAmmo(System.Type weaponType, int ammo)
	{
		Weapon weapon = weapons.Find(x => x.GetType() == weaponType);
		if (weapon != null) 
		{
			return weapon.PickUpAmmo(ammo);
		}
		else return false;
	}
}
