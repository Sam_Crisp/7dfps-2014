﻿using UnityEngine;
using System.Collections;

public abstract class Weapon : MonoBehaviour {
	public abstract void Shoot();
	public abstract void Reload();

	/// <summary>
	/// Handles GUI visualisation of ammo count.
	/// </summary>
	/// <returns>The text to be displayed in the ammo area of the GUI.</returns>
	public abstract string AmmoText();

	/// <summary>
	/// Picks up ammo.
	/// </summary>
	/// <returns><c>true</c>, if up ammo was able to be picked up (i.e. not at max(, <c>false</c> otherwise, in which case the pickup object will not be consumed.</returns>
	/// <param name="count">How much ammo to be picked up.</param>
	public abstract bool PickUpAmmo(int count);

//	public abstract void Holster();
//	public abstract void Equip();
}
