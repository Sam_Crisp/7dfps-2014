﻿using UnityEngine;
using System.Collections;

public class Perfume : Weapon {

	public override void Shoot ()
	{
		// Do nothing.
	}

	public override void Reload ()
	{
		// Do nothing.
	}

	public override string AmmoText ()
	{
		return "";
	}

	public override bool PickUpAmmo (int count)
	{
		// Consume the pickup without doing anything.
		return true;
	}
}
