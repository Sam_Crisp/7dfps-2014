﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SphereCollider))]
public class WeaponPickup : MonoBehaviour {

	// If the player already has this weapon, this much ammo is given instead
	public int ammoPickUpFallback = 6;
	public Transform weaponPrefab;

	void OnTriggerEnter(Collider col)
	{
		// Consumes the item if it can be picked up
		WeaponController controller = col.GetComponentInChildren<WeaponController>();
		if (controller != null && controller.PickUpWeapon(weaponPrefab, ammoPickUpFallback))
			Destroy(gameObject);
	}
}
