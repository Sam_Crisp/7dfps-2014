﻿using UnityEngine;
using System.Collections;

public class Pistol : Weapon {

	public int bulletsPerClip = 6;
	public int clips = 10;
	public int maxClips = 10;
	public float range = 100.0f;
	public float force = 10.0f;
	public float damage = 10.0f;
	public float fireRate = 0.5f;
	public float reloadTime = 0.5f;
	public Transform particlePrefab;

	private float _lastShotTime;
	private int bullets = 6;
	bool reloading = false;
	const int layerMask = 1 << 11 | 1 << 14; // Only environment and damage recievers

	public override void Shoot ()
	{
		print ("Shoot!");
		// Check if the player has ammo
		if (bullets == 0 || reloading || Time.time < _lastShotTime + fireRate) return;

		// Muzzle flash

		Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width * 0.5f, Screen.height * 0.5f));
		ray.origin = transform.position;
		RaycastHit hit;
		Debug.DrawRay(ray.origin, ray.direction, Color.magenta, 2.0f);

		if (Physics.SphereCast(ray, 0.2f, out hit, range, layerMask))
		{
			print ("Hit!");

			// Particle effect 
			Instantiate(particlePrefab, hit.point, Quaternion.LookRotation(hit.normal));

			// Rigidbody knockback
			if (hit.collider.rigidbody)
			{
				hit.collider.rigidbody.AddForceAtPosition(ray.direction * force, hit.point);
			}

			// Handle damage
			DamageHandler handler = hit.collider.GetComponent(typeof(DamageHandler)) as DamageHandler;
			if (handler != null)
				handler.DoDamage(damage);
		}
		bullets--;
		_lastShotTime = Time.time;
	}

	public override void Reload ()
	{
		if (!reloading) StartCoroutine(DoReload());
	}

	public override string AmmoText ()
	{
		return bullets.ToString() + "|" + clips.ToString();
	}

	public override bool PickUpAmmo (int count)
	{
		// Can't pick up if at max
		if (clips == maxClips) return false;
		clips += count;
		if (clips > maxClips) clips = maxClips;
		return true;
	}

	IEnumerator DoReload()
	{
		if (clips > 0 && bullets < bulletsPerClip) {
			reloading = true;
			//yield return new WaitForSeconds(reloadTime);
			yield return StartCoroutine(AnimateReload());
			bullets = bulletsPerClip;
			clips--;
			reloading = false;
		}
	}

	IEnumerator AnimateReload()
	{
		print ("Animating");
		Quaternion startRotation = transform.localRotation;
		Quaternion endRotation = Quaternion.Euler(30.0f, 0.0f, 0.0f);
		Vector3 startPosition = transform.localPosition;
		Vector3 endPosition = new Vector3(0.0f, -0.2f, 0.0f);
		AnimationCurve easing = AnimationCurve.EaseInOut(0.0f, 0.0f, 1.0f, 1.0f);
		float reloadTimeHalf = reloadTime * 0.5f;

		float t = 0.0f;
		while (t < 1.0f)
		{
			t += Time.deltaTime / reloadTimeHalf;
			transform.localPosition = Vector3.Lerp(startPosition, endPosition, easing.Evaluate(t));
			transform.localRotation = Quaternion.Lerp(startRotation, endRotation, easing.Evaluate(t));
			yield return null;
		}
		while (t > 0.0f)
		{
			t -= Time.deltaTime / reloadTimeHalf;
			transform.localPosition = Vector3.Lerp(startPosition, endPosition, easing.Evaluate(t));
			transform.localRotation = Quaternion.Lerp(startRotation, endRotation, easing.Evaluate(t));
			yield return null;
		}
	}
}
