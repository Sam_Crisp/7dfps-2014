﻿using UnityEngine;
using System.Collections;

public abstract class Interactable : MonoBehaviour {
	public float range; // How close the player must be to interact
	public abstract string MouseOver();
	public abstract void Interact();
	public abstract bool CanInteract {get;}
}
