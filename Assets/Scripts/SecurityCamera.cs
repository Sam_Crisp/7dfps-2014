﻿using UnityEngine;
using System.Collections;

public class SecurityCamera : MonoBehaviour {

	public float rotationStart = -45.0f;   // What Y degree to begin at
	public float rotationEnd = -45.0f;     // What Y degree to end at
	public float rotationSpeed = 5.0f;     // Degrees per second of movement
	public float endDelay = 1.0f;          // Seconds to pause before reversing direction

	public float boundZ = 30.0f;           // How much it can rotate up and down to look at player

	AlertState state;
	bool direction;
	Transform head;

	public enum AlertState
	{
		Seeking, CaughtSight, Alert, Off
	}

	void Awake () {
		head = transform.Find("Head");
	}

	IEnumerator Seeking()
	{
		while (state == AlertState.Seeking)
		{

			yield return new WaitForSeconds(endDelay);
		}
	}
}
