﻿using UnityEngine;
using System.Collections;

public class AlarmLights : MonoBehaviour {

	public bool on;
	public float rotationSpeed = 10.0f;
	Transform[] lights;

	void Awake()
	{
		// Get references to lights
		lights = new Transform[transform.childCount];
		for (int i=0; i < transform.childCount; i++)
		{
			lights[i] = transform.GetChild(i);
		}
		Set(false);
	}

	void Update()
	{
		if (on)
		{
			foreach (Transform t in lights)
			{
				t.Rotate(0.0f, Time.deltaTime * rotationSpeed, 0.0f);
			}
		}
	}

	public void Set(bool on)
	{
		this.on = on;
		foreach (Transform t in lights)
		{
			t.light.enabled = on;
		}
	}
}
