﻿using UnityEngine;
using System.Collections;

public abstract class MovementAbstract : MonoBehaviour {

	public float speed = 2.0f;
	public float accel = 10.0f;
	public float rotSpeed = 180.0f;
	
	public abstract void Move(Vector3 targetVelocity);
	public abstract void Rotate(Quaternion targetRotation);
}
