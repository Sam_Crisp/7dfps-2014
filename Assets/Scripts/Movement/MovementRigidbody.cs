﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class MovementRigidbody : MovementAbstract {

	protected void Reset ()
	{
		rigidbody.useGravity = false;
		rigidbody.freezeRotation = true;
	}

	public override void Move(Vector3 targetVelocity)
	{
		if(targetVelocity.sqrMagnitude > 1)
			targetVelocity.Normalize();

		targetVelocity *= speed;

		Vector3 acceleration = (targetVelocity - rigidbody.velocity) * accel;

		if (acceleration.sqrMagnitude > accel)
			acceleration = acceleration.normalized * accel;

		rigidbody.velocity += acceleration * Time.deltaTime;
	}

	public override void Rotate(Quaternion targetRotation) {
		transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotSpeed * Time.deltaTime);
	}
}
