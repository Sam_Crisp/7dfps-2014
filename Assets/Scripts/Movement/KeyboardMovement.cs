﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MovementAbstract))]
public class KeyboardMovement : MonoBehaviour {

	private MovementAbstract _movement;

	protected void Awake () {
		_movement = GetComponent<MovementAbstract>();
	}
	
	void Update () {
		Vector3 targetVelocity = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
		_movement.Move(targetVelocity);
	}
}
