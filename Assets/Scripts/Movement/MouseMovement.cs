﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MovementAbstract))]
public class MouseMovement : MonoBehaviour {

	protected MovementAbstract _movement;
	protected Vector3 _targetPoint;

	protected void Awake ()
	{
		_movement = GetComponent<MovementAbstract>();
	}
	
	protected void Start () {
		_targetPoint = transform.position;
	}

	protected void CheckNewTarget()
	{
		if (Input.GetMouseButtonDown(1)) {
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			_targetPoint = ray.GetPoint(Camera.main.transform.position.y);
			_targetPoint.y = 0.0f;
		}
	}

	protected void UpdatePosition()
	{
		Vector3 targetDirection = _targetPoint - rigidbody.position;
		_movement.Move (targetDirection);
	}

	protected void UpdateRotation()
	{
		if (Vector3.Distance(_targetPoint, transform.position) < 0.5f) return;
		Quaternion targetRotation = Quaternion.LookRotation(_targetPoint - transform.position);
		_movement.Rotate(targetRotation);
	}

	protected void Update ()
	{
		CheckNewTarget();
		UpdatePosition();
		UpdateRotation();
	}

}
