﻿using UnityEngine;
using System.Collections;

public class NavMeshUtil {

	public static Transform FindTargetWithShortestTravelDistance(Transform agent, Transform[] targets)
	{
		Transform closest = null;
		float closestDistance = Mathf.Infinity;
		foreach(Transform t in targets)
		{
			NavMeshPath path = new NavMeshPath();

			// If it can be reached
			if (NavMesh.CalculatePath(agent.position, t.position, -1, path))
			{
				Vector3? previous = null;
				float cumulativeDistance = 0.0f;
				foreach (Vector3 point in path.corners)
				{
					if (!previous.HasValue) {
						previous = point;
						continue;
					}

					cumulativeDistance += Vector3.Distance(previous.Value, point);
				}
				if (cumulativeDistance < closestDistance)
				{
					closest = t;
					closestDistance = cumulativeDistance;
				}
			}
		}
		return closest;
	}

	public static Vector3? FindPointWithShortestTravelDistance(Transform agent, Vector3[] targets)
	{
		Vector3? closest = null;
		float closestDistance = Mathf.Infinity;
		foreach(Vector3 t in targets)
		{
			NavMeshPath path = new NavMeshPath();
			
			// If it can be reached
			if (NavMesh.CalculatePath(agent.position, t, -1, path))
			{
				Vector3? previous = null;
				float cumulativeDistance = 0.0f;
				foreach (Vector3 point in path.corners)
				{
					if (!previous.HasValue) {
						previous = point;
						continue;
					}
					
					cumulativeDistance += Vector3.Distance(previous.Value, point);
				}
				if (cumulativeDistance < closestDistance)
				{
					closest = t;
					closestDistance = cumulativeDistance;
				}
			}
		}
		return closest;
	}
}
