﻿using UnityEngine;

public interface DamageHandler {
	void DoDamage(float damage);
}