﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {

	public float maxHealth = 100.0f;
	public UnityEngine.UI.Text text;

	public bool wasShot {get; private set;}
	public bool isDead {get; private set;}
	public float health {get; private set;}

	protected Animator anim;
	protected LocationalDamageHandler[] locationalColliders;

	void Awake()
	{
		health = maxHealth;
		anim = GetComponent<Animator>();
		locationalColliders = GetComponentsInChildren<LocationalDamageHandler>();
	}

	void Update()
	{
		if (text != null)
			text.text = health.ToString();
	}

	public void Damage(float damage, DamageLocation location)
	{
		if (isDead) return;

		wasShot = true;
		health -= damage;

		// If dead
		if (health <= 0.0f)
			Die();

		// Hit animations
		if (anim != null)
		{
			switch (location)
			{
			case DamageLocation.Head:
				anim.SetTrigger("Hit Head");
				break;
			case DamageLocation.Foot:
				anim.SetTrigger("Hit Foot");
				break;
			case DamageLocation.Back:
				anim.SetTrigger("Hit Back");
				break;
			case DamageLocation.Stomach:
				anim.SetTrigger("Hit Stomach");
				break;
			case DamageLocation.Chest:
			case DamageLocation.Default:
			default:
				anim.SetTrigger("Hit Chest");
				break;
			}
		}
	}

	void Die()
	{
		isDead = true;
		if (anim != null)
			anim.SetBool("Dead", true);

		// Turn off damage colliders
		foreach (LocationalDamageHandler d in locationalColliders)
		{
			d.collider.enabled = false;
		}
		//Destroy(gameObject);
	}
}
