﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(Health)), CanEditMultipleObjects()]
public class HealthEditor : Editor {

	public override void OnInspectorGUI ()
	{
		Health health = target as Health;
		DrawDefaultInspector();
		EditorGUILayout.LabelField("Current Health", health.health.ToString());
	}
}
