﻿using UnityEngine;
using System.Collections;

namespace AISteering {
	public abstract class AISteeringTargeter : MonoBehaviour, System.IComparable<AISteeringTargeter> {

		public int priority;
		public bool on = true;

		public abstract Goal GetGoal();

		public int CompareTo(AISteeringTargeter obj)
		{
			return priority.CompareTo(obj.priority);
		}
	}
}