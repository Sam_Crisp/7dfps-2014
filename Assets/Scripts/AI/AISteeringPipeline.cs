﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace AISteering {
	public class AISteeringPipeline : MonoBehaviour {

		public Dictionary<AISteeringTargeter, bool[]> targeterDebugInfo;
		
		public AISteeringTargeter[] targeters {get; protected set;}
		protected AISteeringActuator actuator;
		
		void Awake () {
			actuator = GetComponent<AISteeringActuator>();
			targeters = GetComponents<AISteeringTargeter>();
			System.Array.Sort(targeters);
			targeterDebugInfo = new Dictionary<AISteeringTargeter, bool[]>();
		}

		void Update ()
		{
			Goal goal = new Goal();

			foreach (AISteeringTargeter targeter in targeters)
			{
				if (targeter.on) 
				{
					Goal update = targeter.GetGoal();
					goal.UpdateChannels(update);
					// Update debug info
					targeterDebugInfo[targeter] = new bool[4] { update.hasPosition, update.hasVelocity, update.hasOrientation, update.hasRotation };
				}
			}

			actuator.DoSteering(goal);
		}
	}
}