﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(LineOfSight))]
public class LineOfSightEditor : Editor {

	LineOfSight los;

	public override void OnInspectorGUI ()
	{
		los = target as LineOfSight;
		DrawDefaultInspector();
		EditorGUILayout.LabelField("Angle to player", los.debugFOV.ToString());
	}
}
