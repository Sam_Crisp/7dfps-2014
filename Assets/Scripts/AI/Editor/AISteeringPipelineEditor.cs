﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using AISteering;

[CustomEditor(typeof(AISteeringPipeline))]
public class AISteeringPipelineEditor : Editor {

	AISteeringPipeline pipeline;

	public override void OnInspectorGUI ()
	{
		pipeline = target as AISteeringPipeline;

		// Draw default
		DrawDefaultInspector();

		if (!pipeline.enabled)
		{
			EditorGUILayout.HelpBox("Steering pipeline disabled", MessageType.Info);
			return;
		}

		// Display each targeter
		if (Application.isPlaying) {
			EditorGUILayout.PrefixLabel("Targeters");
			EditorGUI.indentLevel++;
			if (pipeline.targeters.Length > 0) {

				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.PrefixLabel("");
				EditorGUILayout.PrefixLabel("Position");
				EditorGUILayout.PrefixLabel("Velocity");
				EditorGUILayout.PrefixLabel("Orientation");
				EditorGUILayout.PrefixLabel("Rotation");
				EditorGUILayout.EndHorizontal();

				foreach (AISteeringTargeter targeter in pipeline.targeters)
				{
					if (!targeter.on)
					{
						EditorGUILayout.LabelField(targeter.GetType().Name, "(off)");
						continue;
					}

					EditorGUILayout.BeginHorizontal();
					EditorGUILayout.PrefixLabel(targeter.GetType().Name);
					try {
						bool[] has = pipeline.targeterDebugInfo[targeter];
						EditorGUILayout.Toggle(has[0]);
						EditorGUILayout.Toggle(has[1]);
						EditorGUILayout.Toggle(has[2]);
						EditorGUILayout.Toggle(has[3]);
					}
					catch (System.Collections.Generic.KeyNotFoundException e)
					{
						EditorGUILayout.HelpBox(e.Message, MessageType.Warning);
					}
					EditorGUILayout.EndHorizontal();
				}
			}
			else EditorGUILayout.LabelField("none");
			EditorGUI.indentLevel--;
		}
	}
}
