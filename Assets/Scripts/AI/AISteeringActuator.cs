﻿using UnityEngine;
using System.Collections;

namespace AISteering{
	public class AISteeringActuator : MonoBehaviour {

		public bool walk = false;
		public bool crouch = false;
		public bool dance = false;
		public Transform lookTarget { get; set; }

		const int layerMask = ~(1 << 14 | 1 << 13); // Ignore damage and interactable layers
		const float half = 0.5f;
		bool crouching;
		float speed;
		float originalHeight;
		CapsuleCollider capsule;
		protected NavMeshAgent nav;
		protected Animator anim;
		float lookBlend;
		Vector3 currentLookPos;

		protected void Awake()
		{
			nav = GetComponent<NavMeshAgent>();
			anim = GetComponent<Animator>();
			capsule = collider as CapsuleCollider;
			speed = nav.speed;
			originalHeight = capsule.height;
		}

		public void DoSteering(Goal goal)
		{
			if (goal.hasPosition)
			{
				float crouchSpeed = crouching ? speed * half : speed;
				nav.speed = walk ? speed * half : crouchSpeed;
				nav.destination = goal.position;
			}
			else {
				nav.Stop();
				nav.velocity = Vector3.zero;
			}

			PreventStandingInLowHeadroom();
			UpdateAnimator();
		}

		void UpdateAnimator()
		{
			Vector3 localMove = transform.InverseTransformDirection (nav.velocity / speed);
			float turnAmount = Mathf.Atan2 (localMove.x, localMove.z);
			float forwardAmount = localMove.z;

			anim.SetFloat ("Forward", forwardAmount, 0.1f, Time.deltaTime);
			anim.SetFloat ("Turn", turnAmount, 0.1f, Time.deltaTime);
			anim.SetBool ("Crouch", crouching);
			anim.SetBool ("Dancing", dance);
		}

		void OnAnimatorIK(int layerIndex)
		{
			// if a transform is assigned as a look target, it overrides the vector lookPos value
			if (lookTarget != null) {
				currentLookPos = lookTarget.position;
				lookBlend = Mathf.Lerp(lookBlend, 1.0f, Time.deltaTime * 5.0f);
			}
			else lookBlend = Mathf.Lerp(lookBlend, 0.0f, Time.deltaTime *  5.0f);

			// we set the weight so most of the look-turn is done with the head, not the body.
			anim.SetLookAtWeight(lookBlend, 0.6f, 1.5f);

			// Used for the head look feature.
			anim.SetLookAtPosition( currentLookPos );
		}

		void PreventStandingInLowHeadroom ()
		{
			// prevent standing up in crouch-only zones
			if (!crouch) {
				Ray crouchRay = new Ray (rigidbody.position + Vector3.up * capsule.radius * half, Vector3.up);
				float crouchRayLength = originalHeight - capsule.radius * half;
				crouching = Physics.SphereCast (crouchRay, capsule.radius * half, crouchRayLength, layerMask);
			}
			else crouching = true;
		}	
	}
}