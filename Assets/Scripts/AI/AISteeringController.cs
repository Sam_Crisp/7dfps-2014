﻿using UnityEngine;
using System.Collections;
using AISteering;

public class AISteeringController : MonoBehaviour {

	#region Init
	Targeters targeters;
	AISteeringActuator actuator;
	AISteeringPipeline pipeline;

	class Targeters
	{
		public SeekTarget seekTarget;
		public SeekPoint seekPoint;

		public Targeters(Component component)
		{
			seekTarget = component.GetComponent<SeekTarget>();
			seekPoint = component.GetComponent<SeekPoint>();
		}
	}

	void Awake()
	{
		targeters = new Targeters(this);
		actuator = GetComponent<AISteeringActuator>();
		pipeline = GetComponent<AISteeringPipeline>();
	}
	#endregion

	#region SeekTarget
	public void SeekTargetOn(Transform target, float range)
	{
		SeekTargetOn(target);
		targeters.seekTarget.range = range;
	}

	public void SeekTargetOn(Transform target)
	{
		targeters.seekTarget.on = true;
		targeters.seekTarget.target = target;
	}

	public void SeekTargetOff()
	{
		targeters.seekTarget.on = false;
	}
	#endregion

	#region SeekPoint
	public void SeekPointOn(Vector3 target, float range)
	{
		SeekPointOn(target);
		targeters.seekPoint.range = range;
	}
	
	public void SeekPointOn(Vector3 target)
	{
		targeters.seekPoint.on = true;
		targeters.seekPoint.target = target;
	}
	
	public void SeekPointOff()
	{
		targeters.seekPoint.on = false;
	}
	#endregion

	#region Actuator

	public void Crouch()
	{
		actuator.crouch = true;
	}

	public void Stand()
	{
		actuator.crouch = false;
	}

	public void Run()
	{
		actuator.walk = false;
	}

	public void Walk()
	{
		actuator.walk = true;
	}

	public void LookAt(Transform target = null)
	{
		actuator.lookTarget = target;
	}

	public void Disable()
	{
		pipeline.enabled = false;
	}

	public void Enable()
	{
		pipeline.enabled = true;
	}

	#endregion
}
