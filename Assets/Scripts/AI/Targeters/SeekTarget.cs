﻿using UnityEngine;
using System.Collections;

namespace AISteering {
	public class SeekTarget : AISteeringTargeter {

		public Transform   target;  // What to seek
		public float       range;   // How close to get

		public override Goal GetGoal ()
		{
			Goal goal = new Goal();
			if (target == null) return goal;
			if (Vector3.Distance(transform.position, target.position) > range) {
				goal.position = target.position;
				goal.hasPosition = true;
			}
			return goal;
		}
	}
}