﻿using UnityEngine;
using System.Collections;

namespace AISteering {
	public class SeekPoint : AISteeringTargeter {
		
		public Vector3     target;  // What to seek
		public float       range;   // How close to get
		
		public override Goal GetGoal ()
		{
			Goal goal = new Goal();
			if (Vector3.Distance(transform.position, target) > range) {
				goal.position = target;
				goal.hasPosition = true;
			}
			return goal;
		}
	}
}