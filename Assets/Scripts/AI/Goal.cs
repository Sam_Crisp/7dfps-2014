﻿using UnityEngine;

namespace AISteering {
	public struct Goal
	{
		public bool hasPosition, hasOrientation, hasVelocity, hasRotation;
		
		public Vector3 position;
		public Quaternion orientation;
		public Vector3 velocity;
		public Quaternion rotation;
		
		public void UpdateChannels(Goal o)
		{
			if (o.hasPosition) {
				position = o.position;
				hasPosition = true;
			}
			if (o.hasOrientation) {
				orientation = o.orientation;
				hasOrientation = true;
			}
			if (o.hasVelocity) {
				velocity = o.velocity;
				hasVelocity = true;
			}
			if (o.hasRotation) {
				rotation = o.rotation;
				hasRotation = true;
			}
		}

		public bool canMergeGoals(Goal o)
		{
			return !(
				(hasPosition && o.hasPosition) ||
				(hasRotation && o.hasRotation) ||
				(hasVelocity && o.hasVelocity) ||
				(hasOrientation && o.hasOrientation)
				);
		}
	}
}