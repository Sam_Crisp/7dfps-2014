﻿using UnityEngine;
using System.Collections;

public class LineOfSight : MonoBehaviour {

	public float maxSightDistance = 20.0f;
	public float FOV = 45.0f;

	CapsuleCollider capsule;
	float height;
	float FOVhalf;
	int layerMask = ~(1 << 9 | 1 << 14); // All but characters and damage

	[HideInInspector]
	public float debugFOV;

	protected void Awake()
	{
		capsule = collider as CapsuleCollider;
		height = capsule.height;
		FOVhalf = FOV * 0.5f;
	}

	bool HasLineOfSightOfPlayer(Vector3 dirToPlayer, Vector3 headPos)
	{
		RaycastHit hit;
		//Debug.DrawRay(rigidbody.position + Vector3.up * height, dirToPlayer, Color.red);
		if (Physics.Raycast(rigidbody.position + Vector3.up * height, dirToPlayer, out hit, maxSightDistance, layerMask))
		{
			// Hit! Check if it is the player.
			Debug.DrawLine(rigidbody.position + Vector3.up * height, hit.point, Color.red);
			return hit.collider.CompareTag("Player");
		}
		return false;
	}

	bool CheckInsideConeOfVision(Vector3 dirToPlayer)
	{
		Vector3 flatDirToPlayer = new Vector3(dirToPlayer.x, 0.0f, dirToPlayer.z);
		return (debugFOV = Vector3.Angle(transform.forward, flatDirToPlayer.normalized)) < FOVhalf;
	}

	public bool CanSeePlayer
	{
		get
		{
			Vector3 headPos = rigidbody.position + Vector3.up * height;
			Vector3 dirToPlayer = Game.Player.position - headPos;
			return HasLineOfSightOfPlayer(dirToPlayer, headPos) && CheckInsideConeOfVision(dirToPlayer);
		}
	}

	void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.yellow;
		Gizmos.DrawRay(rigidbody.position, Quaternion.Euler(0.0f, FOVhalf, 0.0f) * transform.forward);
		Gizmos.DrawRay(rigidbody.position, Quaternion.Euler(0.0f, -FOVhalf, 0.0f) * transform.forward);
	}
}
