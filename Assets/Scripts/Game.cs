﻿using UnityEngine;
using System.Collections;

public class Game : MonoBehaviour {

	public static Transform Player;
	public static Transform PlayerHead;

	void Awake()
	{
		Player = GameObject.FindGameObjectWithTag("Player").transform;
		PlayerHead = Player.GetChild(0);
	}
}
