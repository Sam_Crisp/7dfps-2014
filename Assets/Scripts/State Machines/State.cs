﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(StateMachine))]
public abstract class State : MonoBehaviour {
	public abstract void Action();
	public abstract void EntryAction();
	public abstract void ExitAction();

	public List<Transition> transitions = new List<Transition>();
}
