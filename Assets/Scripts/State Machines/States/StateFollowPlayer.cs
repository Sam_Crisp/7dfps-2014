﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(AISteering.SeekTarget))]
public class StateFollowPlayer : State {

	public float range = 3.0f;

	protected AISteeringController steering;
	protected LineOfSight lineofsight;

	protected Transition pressedSpaceToIdle;
	bool IsTriggered()
	{
		return Input.GetKeyDown(KeyCode.P);
	}

	protected void Awake()
	{
		steering = GetComponent<AISteeringController>();
		lineofsight = GetComponent<LineOfSight>();
		transitions = new List<Transition>();

		pressedSpaceToIdle = new Transition(IsTriggered, null, GetComponent<StatePatrol>());

		transitions.Add(pressedSpaceToIdle);
	}

	public override void Action ()
	{
		// Do nothing.
	}

	public override void EntryAction ()
	{
		steering.SeekTargetOn(Game.Player, range);
		steering.Run();
		steering.LookAt(Game.PlayerHead);
	}

	public override void ExitAction ()
	{
		steering.SeekTargetOff();
		steering.LookAt();
	}
}
