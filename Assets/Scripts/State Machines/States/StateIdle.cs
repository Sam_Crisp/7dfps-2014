﻿using UnityEngine;
using System.Collections.Generic;

public class StateIdle : State {

	LineOfSight lineofsight;

	protected Transition pressedSpaceToFollow;
	bool IsTriggered()
	{
		return lineofsight.CanSeePlayer || Input.GetKeyDown(KeyCode.P);
	}
	
	protected void Awake()
	{
		transitions = new List<Transition>();
		lineofsight = GetComponent<LineOfSight>();
		
		pressedSpaceToFollow = new Transition(IsTriggered, null, GetComponent<StateSetOffAlarms>());
		
		transitions.Add(pressedSpaceToFollow);
	}

	public override void Action ()
	{
		// Do nothing.
	}

	public override void EntryAction ()
	{
		// Do nothing.
	}

	public override void ExitAction ()
	{
		// Do nothing.
	}
}
