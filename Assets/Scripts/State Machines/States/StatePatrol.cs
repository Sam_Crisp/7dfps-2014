﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StatePatrol : State {

	public Vector3[] nodes;
	public PatrolType patrolType;
	public float delay = 0.0f; // How long to pause at each node before continuing
	public float range = 0.0f; // How close to node they must reach

	private Vector3 currentTarget;
	private bool forwards = true;
	private int currentNodeIndex = 0;
	private bool isPausing = false;

	protected AISteeringController steering;
	protected LineOfSight lineofsight;
	protected Health health;

	public enum PatrolType
	{
		Loop, PingPong
	}

	// Transition -> See Player
	protected Transition seesPlayer;
	bool IsTriggered()
	{
		return lineofsight.CanSeePlayer || health.wasShot || AlarmButton.Alarm;
	}

	// Transition -> Alarms go off
	protected Transition alarmsTriggered;
	bool AlarmsIsTriggered()
	{
		return AlarmButton.Alarm;
	}

	protected void Awake()
	{
		steering = GetComponent<AISteeringController>();
		lineofsight = GetComponent<LineOfSight>();
		health = GetComponent<Health>();

		transitions = new List<Transition>();
		seesPlayer = new Transition(IsTriggered, null, GetComponent<StateSetOffAlarms>());
		alarmsTriggered = new Transition(AlarmsIsTriggered, null, GetComponent<StateFollowPlayer>());
		transitions.Add(seesPlayer);
	}

	IEnumerator Delay(float seconds)
	{
		if (!isPausing) {
			steering.SeekPointOff();
			isPausing = true;
			yield return new WaitForSeconds(seconds);
			isPausing = false;
			steering.SeekPointOn(currentTarget, range);
		}
	}

	public override void Action ()
	{
		// Check if arrived at node
		if (!isPausing && transform.position == currentTarget)
		{
			// Move to next node
			currentNodeIndex = currentNodeIndex + (forwards ? 1 : -1);

			// Handle cycling
			if (currentNodeIndex == nodes.Length || currentNodeIndex == -1)
			{
				// If looping, go back to first node
				if (patrolType == PatrolType.Loop)
					currentNodeIndex = 0;
				// If ping-ponging, reverse direction and jump back 2 nodes
				else if (patrolType == PatrolType.PingPong) {
					forwards = !forwards;
					currentNodeIndex = currentNodeIndex + (forwards ? 2 : -2);
				}
			}
			// Set new target
			currentTarget = nodes[currentNodeIndex];
			StartCoroutine(Delay(delay));
			//steering.SeekPointOn(currentTarget, 0.0f);
		}
	}

	public override void EntryAction ()
	{
		// Get closest node
		Vector3? closestTarget = NavMeshUtil.FindPointWithShortestTravelDistance(transform, nodes);
		if (closestTarget.HasValue)
			currentTarget = closestTarget.Value;
		else Debug.LogError("Couldn't find closest node in patrol route.");
		steering.SeekPointOn(currentTarget, range);

		// Walk
		steering.Walk();
	}

	public override void ExitAction ()
	{
		steering.SeekPointOff();
	}
}
