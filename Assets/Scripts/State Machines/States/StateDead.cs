﻿using UnityEngine;
using System.Collections;

public class StateDead : State {

	protected NavMeshAgent navmeshagent;
	protected AISteeringController steering;

	protected void Awake()
	{		
		steering = GetComponent<AISteeringController>();
		navmeshagent = GetComponent<NavMeshAgent>();
	}

	public override void Action ()
	{
		// Do nothing.
	}

	public override void EntryAction ()
	{
		collider.enabled = false;
		navmeshagent.enabled = false;
		steering.Disable();
	}

	public override void ExitAction ()
	{
		// Do nothing.
	}
}
