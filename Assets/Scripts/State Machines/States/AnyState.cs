﻿using UnityEngine;
using System.Collections.Generic;

public class AnyState : MonoBehaviour {

	public List<Transition> transitions;

	protected Health health;

	Transition die;
	bool IsTriggered()
	{
		return health.isDead;
	}

	
	void Awake()
	{
		health = GetComponent<Health>();
		die = new Transition(IsTriggered, null, GetComponent<StateDead>());
		transitions = new List<Transition>();
		transitions.Add(die);
	}
}
