﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StateSetOffAlarms : State {

	protected AlarmButton[] buttons;
	protected AlarmButton currentTargetButton;
	protected AISteeringController steering;
	
	protected Transition pressedAlarm;
	bool IsTriggered()
	{
		// If the agent or someone else set off the alarm
		return currentTargetButton.pressed || AlarmButton.Alarm;
	}
	
	protected void Awake()
	{
		buttons = FindObjectsOfType<AlarmButton>();
		steering = GetComponent<AISteeringController>();
		transitions = new List<Transition>();
		
		pressedAlarm = new Transition(IsTriggered, null, GetComponent<StateFollowPlayer>());
		
		transitions.Add(pressedAlarm);
	}

	public override void Action ()
	{
		// Press button when within range
		if (Vector3.Distance(currentTargetButton.transform.position, transform.position) < 1.0f)
			currentTargetButton.SetOffAlarm();
		// TODO: Play animation of character pressing button
	}

	public override void EntryAction ()
	{
		print ("Doing entry");
		// if there are not buttons in the scene, do nothing
		if (buttons.Length == 0) return;

		// Find nearest alarm button
		Transform[] transforms = new Transform[buttons.Length];
		for (int i = 0; i < buttons.Length; i++)
		{
			transforms[i] = buttons[i].transform;
		}
		Transform closest = NavMeshUtil.FindTargetWithShortestTravelDistance(transform, transforms);
		currentTargetButton = closest.GetComponent<AlarmButton>();
//		AlarmButton closestButton = buttons[0];
//		float closestDistance = Mathf.Infinity;
//		foreach(AlarmButton b in buttons)
//		{
//			if (Vector3.Distance(transform.position, b.transform.position) < closestDistance)
//			{
//				closestButton = b;
//				closestDistance = Vector3.Distance(transform.position, b.transform.position);
//			}
//		}
//		currentTargetButton = closestButton;

		// Move to button
		//steering.SeekTargetOn(closestButton.transform, 0.0f);
		steering.SeekTargetOn(closest, 0.0f);

		// Run
		steering.Run();
	}

	public override void ExitAction ()
	{
		steering.SeekTargetOff();
	}
}
