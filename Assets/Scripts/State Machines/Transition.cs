﻿using UnityEngine;
using System.Collections;

public class Transition {
	public delegate bool TriggerCheck();

	public TriggerCheck IsTriggered;
	public System.Action Action;
	public State TargetState;

	public Transition(TriggerCheck isTriggered, System.Action action, State targetState)
	{
		IsTriggered = isTriggered;
		Action = action;
		TargetState = targetState;
	}
}
