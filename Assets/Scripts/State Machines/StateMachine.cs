﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AnyState))]
public class StateMachine : MonoBehaviour {

	[HideInInspector]
	public State initialState;

	public State[] states {get; private set;}
	public State currentState {get; private set;}

	protected AnyState anyState;

	void OnValidate()
	{
		states = GetComponents<State>();
	}

	void Start()
	{
		states = GetComponents<State>();
		anyState = GetComponent<AnyState>();
		currentState = initialState;
		if (currentState != null) currentState.EntryAction();
	}

	void Update()
	{
		// If no state, don't do anything
		if (currentState == null) return;

		// Assume no transition is triggered
		Transition triggeredTransition = null;

		// Check each transition and store the first one that triggers
		foreach (Transition transition in currentState.transitions)
		{
			if (transition.IsTriggered())
			{
				triggeredTransition = transition;
				break;
			}
		}

		// Check transition that can occur from any state
		foreach (Transition transition in anyState.transitions)
		{
			if (transition.IsTriggered())
			{
				triggeredTransition = transition;
				break;
			}
		}

		// Check if we have a transition to fire
		if (triggeredTransition != null) {
			// Find the target state
			State targetState = triggeredTransition.TargetState;

			// Add the exit action of the old state,
			// the transition action,
			// and the entry for the new state
			System.Action actions = currentState.ExitAction;
			actions += triggeredTransition.Action;
			actions += targetState.EntryAction;

			// Complete the transition and return do the actions
			currentState = targetState;
			actions.Invoke();
		}

		else currentState.Action();
	}
}
