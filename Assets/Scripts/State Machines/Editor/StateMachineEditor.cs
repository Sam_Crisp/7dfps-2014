﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(StateMachine))]
public class StateMachineEditor : Editor {

	StateMachine sm;

	public override void OnInspectorGUI ()
	{
		// Get reference to state machine
		sm = target as StateMachine;

		// Get references to state components
		State[] states = sm.GetComponents<State>();

		// Get current initial state
		int _choiceIndex = System.Array.IndexOf<State>(states, sm.initialState) + 1;

		// Get an array of strings of state names
		string[] choices = new string[states.Length+1];
		choices[0] = "none";
		int i = 1;
		foreach (State s in states)
		{
			choices[i] = s.GetType().Name;
			i++;
		}

		// Draw default
		DrawDefaultInspector();

		// Drop down menu for initial state
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.PrefixLabel("Initial State");
		_choiceIndex = EditorGUILayout.Popup(_choiceIndex, choices);
		EditorGUILayout.EndHorizontal();

		// If game is running, display current state
		if (Application.isPlaying)
			EditorGUILayout.LabelField("Current State", sm.currentState == null ? "none" : sm.currentState.GetType().Name);

		// Set initial state to selection
		if (_choiceIndex > 0)
			sm.initialState = states[_choiceIndex-1];
		else sm.initialState = null;
	}
}
