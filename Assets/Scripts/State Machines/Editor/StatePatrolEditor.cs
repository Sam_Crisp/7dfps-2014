﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(StatePatrol))]
public class StatePatrolEditor : Editor {

	private static Vector3 pointSnap = Vector3.one * 0.1f;

	private NavMeshPath[] paths;

	void OnSceneGUI()
	{
		StatePatrol patrol = target as StatePatrol;

		// Draw a line between each node
		//Handles.DrawAAPolyLine(patrol.nodes);

		if (patrol.nodes.Length > 1) {
			paths = new NavMeshPath[patrol.nodes.Length-1];
			for (int i = 1; i < patrol.nodes.Length; i++)
			{
				paths[i-1] = new NavMeshPath();
				if (NavMesh.CalculatePath(patrol.nodes[i-1], patrol.nodes[i], -1, paths[i-1]))
					Handles.DrawAAPolyLine(paths[i-1].corners);
			}
			if (patrol.patrolType == StatePatrol.PatrolType.Loop) {
				NavMeshPath loop = new NavMeshPath();
				if (NavMesh.CalculatePath(patrol.nodes[patrol.nodes.Length-1], patrol.nodes[0], -1, loop))
					Handles.DrawAAPolyLine(loop.corners);
			}
		}

		// Movable handles for patrol route
		for (int i = 0; i < patrol.nodes.Length; i++)
		{
			// Draw a handle for each patrol node
			Vector3
				oldPoint = patrol.nodes[i],
				newPoint = Handles.FreeMoveHandle(
					oldPoint, Quaternion.identity, 0.5f, pointSnap, Handles.SphereCap);

			// If the player moves the handle
			if (oldPoint != newPoint) {
				// Raycast to find the ground or obstacle dragged over
				Camera editorCamera = SceneView.currentDrawingSceneView.camera;
				Vector2 mousePos = Event.current.mousePosition;
				mousePos = new Vector2(mousePos.x, editorCamera.pixelHeight - mousePos.y);
				Ray ray = editorCamera.ScreenPointToRay(mousePos);
				RaycastHit rhit;
				if (Physics.Raycast(ray, out rhit, 100.0f, (1 << 11)))
				{
					// If there is ground, sample the navmesh for closest walkable position
					NavMeshHit nhit;
					if (NavMesh.SamplePosition(rhit.point, out nhit, 5.0f, -1))
					{
						Undo.RecordObject(patrol, "Move Patrol Node");
						patrol.nodes[i] = nhit.position;
					}
				}
				else 
				{
					// If there is no ground, sample the navmesh for closest walkable position
					NavMeshHit nhit;
					if (NavMesh.SamplePosition(newPoint, out nhit, 5.0f, -1))
					{
						Undo.RecordObject(patrol, "Move Patrol Node");
						patrol.nodes[i] = nhit.position;
					}
				}
			}
		}
	}
}
