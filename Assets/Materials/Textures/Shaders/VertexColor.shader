﻿Shader "Custom/VertexColor" {
	Properties {
		_Tint ("Tint", COLOR) = (1.0, 1.0, 1.0, 1.0)
		_EmissionFactor ("Emission Factor", Float) = 1.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert

		float4 _Tint;
		float _EmissionFactor;

		struct Input {
			float4 color : COLOR;
			float3 worldPos;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			half3 c = IN.color.rgb;
			o.Albedo = c * _Tint;
			o.Emission = c * IN.color.a * _EmissionFactor;
			o.Alpha = IN.color.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
